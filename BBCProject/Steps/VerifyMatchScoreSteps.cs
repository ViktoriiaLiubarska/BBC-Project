﻿using BBCProject.pages;
using BBCProject.Pages;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using TechTalk.SpecFlow;

namespace BBCProject.Steps
{
    [Binding]
    public class VerifyMatchScoreSteps
    {
        private readonly IWebDriver driver = Webdriver.Driver;

        [When(@"User goes to the Scores & Fixtures page")]
        public void WhenUserGoesToTheScoresFixturesPage()
        {
            new HomePage(driver).SportTabClick()
                .FootballTabClick()
                .ScoresFixturesTabClick();
        }

        [When(@"User searches for '(.*)' scoreboard in '(.*)'")]
        public void WhenUserSearchesForScoreboardIn(string championship, string month)
        {
            var scoresFixturesPage = new ScoresFixturesPage(driver).Search(championship)
                .SelectMonth(month);
            ScenarioContext.Current.Add("pageWithScoreBoard", scoresFixturesPage);
        }

        [Then(@"Team '(.*)' and team '(.*)' played with score '(.*)' : '(.*)'")]
        public void ThenTeamAndTeamPlayedWithScore(string team1, string team2, int score1, int score2)
        {
            var scoresFixturesPage = ScenarioContext.Current.Get<ScoresFixturesPage>("pageWithScoreBoard");
            Score expectedScore = new Score(team1, team2, score1, score2);
            Score actualScore = scoresFixturesPage.GetScore(team1, team2);
            Assert.IsTrue(actualScore.Equals(expectedScore));
        }

        [Given(@"User has found shampionship scoreboard")]
        public void GivenUserHasFoundShampionshipScoreboard()
        {
        }

        [When(@"User clicks on match between '(.*)' and '(.*)'")]
        public void WhenUserClicksOnMatchBetweenAnd(string team1, string team2)
        {
            var scoresFixturesPage = ScenarioContext.Current.Get<ScoresFixturesPage>("pageWithScoreBoard");
            scoresFixturesPage.MatchClick(team1, team2);
        }


        [Then(@"Team '(.*)' and team '(.*)' played with '(.*)' : '(.*)'")]
        public void ThenTeamAndTeamPlayedWith(string team1, string team2, int score1, int score2)
        {
            var actualScore = new MatchDetailsPage(driver).GetScore();
            Score expectedScore = new Score(team1, team2, score1, score2);
            Assert.IsTrue(actualScore.Equals(expectedScore));
        }


    }
}
