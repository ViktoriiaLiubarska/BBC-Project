﻿using BBCProject.pages;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using System.Linq;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace BBCProject.Features
{
    [Binding]
    public class VerifyAticlesTitleSteps
    {
        private readonly IWebDriver driver = Webdriver.Driver;

        [Given(@"User is on homepage")]
        public void GivenUserIsOnHomepage()
        {
        }

        [When(@"User goes to news section")]
        public void WhenUserGoesToNewsSection()
        {
            new HomePage(driver).NewsTabClick();
        }
        
        [Then(@"Title of headline article should be '(.*)'")]
        public void ThenTitleOfHeadlineArticleShouldBe(string title)
        {
            Assert.AreEqual(title, new NewsPage(driver).getHeadlineArticleTitle());
        }

        [Then(@"Titles of secondary articles should be")]
        public void ThenTitlesOfSecondaryArticlesShouldBe(Table table)
        {
            var actual = table.Rows.Select(x => x.Values.ElementAt(0)).ToList();
            Assert.IsTrue(actual.SequenceEqual(new NewsPage(driver).SecondaryArticlesTitles()));
        }

        [When(@"User searches for the text of the category link of the headline article")]
        public void WhenUserSearchesForTheTextOfTheCategoryLinkOfTheHeadlineArticle()
        {
            NewsPage newsPage = new NewsPage(driver);
            newsPage.Search(newsPage.getCategory());
        }

        [Then(@"Title of first article should be '(.*)'")]
        public void ThenTitleOfFirstArticleShouldBe(string title)
        {
            Assert.AreEqual(title, new SearchPage(driver).getFirstArticleTitle());
        }
    }
}
