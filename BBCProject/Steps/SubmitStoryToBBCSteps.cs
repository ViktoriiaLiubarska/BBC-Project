﻿using BBCProject.pages;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace BBCProject.Features
{
    [Binding]
    public class SubmitStoryToBBCSteps
    {
        private readonly IWebDriver driver = Webdriver.Driver;

        [When(@"User goes to the How to share with BBC page")]
        public void WhenUserGoesToTheHowToShareWithBBCPage()
        {
            new HomePage(driver).NewsTabClick()
                .CoronavirusTabClick()
                .YourCoronavirusStoriesClick()
                .HowToShareWithBBCTitleClick();
        }

        [When(@"User submits story to BBC")]
        public void WhenUserSubmitsStoryToBBC(Table table)
        {
            Dictionary<string, string> values = table.Rows.ToDictionary(x=>x.Values.ElementAt(0).Trim('\"'), 
                y=>y.Values.ElementAt(1));
            new HowToShareWithBBCPage(driver).FillForm(values);
            ScenarioContext.Current.Add("formElements", values.Keys.ToList());
        }

        [Then(@"Error message '(.*)' is displayed below field '(.*)'")]
        public void ThenErrorMessageTsDisplayedBelowField(string message, string field)
        {
            var formElements = ScenarioContext.Current.Get<List<string>>("formElements");
            var validationErrors = new HowToShareWithBBCPage(driver).GetValidationErrors(formElements);
            Assert.IsTrue(validationErrors[field.Trim('\"')] == message);
        }
    }
}
