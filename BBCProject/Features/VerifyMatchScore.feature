﻿Feature: Verify match score
Verifying that 2 specific teams played with a specific score

Background: 
Given User is on homepage
When User goes to the Scores & Fixtures page

Scenario Outline: match score on Scores & Fixtures Page
When User searches for '<championship>' scoreboard in '<month>'
Then Team '<team1>' and team '<team2>' played with score '<score1>' : '<score2>'

Examples: 
| championship          | month   | team1              | team2         | score1 | score2 |
| Scottish Championship | 2019-09 | Greenock Morton    | Dundee        | 1      | 0      |
| Scottish Championship | 2020-01 | Queen of the South | Dundee United | 0      | 1      |
| Scottish Championship | 2019-10 | Greenock Morton    | Ayr United    | 2      | 3      |
| Champions League      | 2020-02 | Napoli             | Barcelona     | 1      | 1      |
| Champions League      | 2019-11 | Valencia           | Chelsea       | 2      | 2      |


Scenario Outline: match score on Match Details Page
When User searches for '<championship>' scoreboard in '<month>'
And User clicks on match between '<team1>' and '<team2>'
Then Team '<team1>' and team '<team2>' played with '<score1>' : '<score2>'


Examples: 
| championship          | month   | team1              | team2         | score1 | score2 |
| Scottish Championship | 2019-09 | Greenock Morton    | Dundee        | 1      | 0      |
| Scottish Championship | 2020-01 | Queen of the South | Dundee United | 0      | 1      |
| Scottish Championship | 2019-10 | Greenock Morton    | Ayr United    | 2      | 3      |
| Champions League      | 2020-02 | Napoli             | Barcelona     | 1      | 1      |
| Champions League      | 2019-11 | Valencia           | Chelsea       | 2      | 2      |


