﻿Feature: Verify aticles title
Verifying titles of the news.

Scenario: Title of Headline article
Given User is on homepage
When User goes to news section
Then Title of headline article should be 'New Covid restrictions could last six months - PM'

Scenario: Titles of secondary articles
Given User is on homepage
When User goes to news section
Then Titles of secondary articles should be
| Title                                              |
| Top US scientist says Trump ad quote misleading    |
| China to test whole city for Covid-19 in five days |
| Supreme Court pick vows to 'apply law as written'  |
| Lakers pay tribute to Bryant after NBA title win   |
| Italian teenager could be first millennial saint   |

Scenario: Title of first article on search result page
Given User is on homepage
When User goes to news section
And User searches for the text of the category link of the headline article
Then Title of first article should be 'World's End: World's End'
