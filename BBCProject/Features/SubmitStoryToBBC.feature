﻿Feature: Submit story to BBC
Verifying validation messages for empty mandatory fields 
of the form Submit your story to BBC

Scenario Outline: Submit story to BBC leaving one of the mandatory fields empty
Given User is on homepage
When User goes to the How to share with BBC page
And User submits story to BBC 
| field                  | value                  |
| "Tell us your story. " | <Tell us your story. > |
| Name                   | <Name>                 |
| I accept the           | <I accept the>         |
| I am over 16           | <I am over 16 >        |
Then Error message '<error message>' is displayed below field '<field>'

Examples: 
| Tell us your story. | Name | I accept the | I am over 16 | error message       | field                  |
|                     | Vika | yes          | yes          | can't be blank      | "Tell us your story. " |
| story               |      | yes          | yes          | Name can't be blank | Name                   |
| story               | Vika | no           | yes          | must be accepted    | I accept the           |
| story               | Vika | yes          | no           | must be accepted    | I am over 16           |

