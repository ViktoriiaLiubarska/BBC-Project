﻿
using BBCProject.pages;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace BBCProject.tests
{
    [TestClass]
    public class FormValidationTests : BaseTest
    {
        [TestMethod]
        public  void CheckValidationForEmptyTellUsYourStoryField()
        {
            var formSettings = new Dictionary<string, string>()
            {
                {"Tell us your story. ", "" },
                {"Name", "Vika"},
                {"I accept the", "yes" },
                {"I am over 16", "yes" },
            };
            new BusinessLogicLayer(driver).SubmitStoryToBBC(formSettings);
            var validationErrors = new HowToShareWithBBCPage(driver).GetValidationErrors(formSettings.Keys.ToList());
            Assert.IsTrue(validationErrors["Tell us your story. "] == "can't be blank");
        }

        [TestMethod]
        public void CheckValidationForEmptyNameField()
        {
            var formSettings = new Dictionary<string, string>()
            {
                {"Tell us your story. ", "Story" },
                {"Name", ""},
                {"I accept the", "yes" },
                {"I am over 16", "yes"},
            };

            new BusinessLogicLayer(driver).SubmitStoryToBBC(formSettings);
            var validationErrors = new HowToShareWithBBCPage(driver).GetValidationErrors(formSettings.Keys.ToList());
            Assert.IsTrue(validationErrors["Name"] == "Name can't be blank");
        }
        
        [TestMethod]
        public void CheckValidationForUncheckedIAmOver16Checkbox()
        {
            var formSettings = new Dictionary<string, string>()
            {
                {"Tell us your story. ", "Story" },
                {"Name", "Vika"},
                {"I accept the", "yes" },
                {"I am over 16", "no"},
            };

            new BusinessLogicLayer(driver).SubmitStoryToBBC(formSettings);
            var validationErrors = new HowToShareWithBBCPage(driver).GetValidationErrors(formSettings.Keys.ToList());
            Assert.IsTrue(validationErrors["I am over 16"] == "must be accepted");
        }

        [TestMethod]
        public void CheckValidationForUncheckedIAcceptTeamsOfServicesCheckbox()
        {
            var formSettings = new Dictionary<string, string>()
            {
                {"Tell us your story. ", "Story" },
                {"Name", "Vika"},
                {"I accept the", "no" },
                {"I am over 16", "yes"},
            };

            new BusinessLogicLayer(driver).SubmitStoryToBBC(formSettings);
            var validationErrors = new HowToShareWithBBCPage(driver).GetValidationErrors(formSettings.Keys.ToList());
            Assert.IsTrue(validationErrors["I accept the"] == "must be accepted");
        }
    }
}
