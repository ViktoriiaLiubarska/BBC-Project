﻿using BBCProject.pages;
using BBCProject.Pages;
using BBCProject.tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace BBCProject.Tests
{
    [TestClass]
    public class ScoreTests : BaseTest
    {
        [TestMethod]
        [DataRow("Scottish Championship", "2019-09", "Greenock Morton", "Dundee", 1, 0)]
        [DataRow("Scottish Championship", "2019-10", "Greenock Morton", "Ayr United", 2, 3)]
        [DataRow("Scottish Championship", "2020-01", "Queen of the South", "Dundee United", 0, 1)]
        [DataRow("Champions League", "2020-02", "Napoli", "Barcelona", 1, 1)]
        [DataRow("Champions League", "2019-11", "Valencia", "Chelsea", 2, 2)]

        public void CheckMatchResult(string championship, string month, string team1, string team2, int score1, int score2)
        {
            Score expectedScore = new Score(team1, team2, score1, score2);
            
            var scoresFixturesPage = new BusinessLogicLayer(driver).SearchForChampionshipScoreBoard(championship, month);
            var actualScore = scoresFixturesPage.GetScore(team1, team2);
            Assert.IsTrue(actualScore.Equals(expectedScore));
            
            var actualScoreOnMatchDetailPage = scoresFixturesPage.MatchClick(team1, team2).GetScore();
            Assert.IsTrue(actualScoreOnMatchDetailPage.Equals(expectedScore));
        }



    }
}
