﻿using BBCProject.pages;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace BBCProject.tests
{
    [TestClass]
    public class ArticlesTitlesTests : BaseTest
    {
        [TestMethod]
        public void CheckHeadlineArticleTitle()
        {
            string expectedHeadlineArticleTitle = "New Covid restrictions could last six months - PM";

            NewsPage newsPage = new HomePage(driver).NewsTabClick();
            Assert.AreEqual(expectedHeadlineArticleTitle, newsPage.getHeadlineArticleTitle());
        }

        [TestMethod]
        public void CheckSecondaryArticleTitles()
        {
            List<string> expectedSecondaryArticles = new List<string>()
            {
                "Oxford vaccine trial paused as participant falls ill",
                "Diplomats surround 'harassed' Belarus Nobel winner",
                "US to withdraw 2,200 troops from Iraq within weeks",
                "Police shoot autistic boy after mum calls for help"
            };

            NewsPage newsPage = new HomePage(driver).NewsTabClick();
            Assert.IsTrue(Utils.IsListsEqual(newsPage.SecondaryArticlesTitles(), expectedSecondaryArticles));
        }

        [TestMethod]
        public void CheckFirstArticleTitleOnSearchPage()
        {
            string expectedFirstArticleTitle = "Panorama: Lockdown UK";

            new BusinessLogicLayer(driver).SearchForTextOfCategoryLinkOfHeadlineArticleOnNewsPage();
            Assert.AreEqual(expectedFirstArticleTitle, new SearchPage(driver).getFirstArticleTitle());
        }
    }
}
