﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;

namespace BBCProject.tests
{
    [TestClass]
    public class BaseTest
    {
        protected readonly IWebDriver driver = Webdriver.Driver;

        [TestInitialize]
        public void SetUp()
        {
            driver.Navigate().GoToUrl("https://www.bbc.com");
        }

        [TestCleanup]
        public void Qiut()
        {
            Webdriver.Quit();
        }
    }
}
