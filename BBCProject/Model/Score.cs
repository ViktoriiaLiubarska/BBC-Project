﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BBCProject
{
    public class Score : IEquatable<Score>
    {
        public int FirstTeamScore { get; set; }
        public int SecondTeamScore { get; set; }
        public string FirstTeam { get; set; }
        public string SecondTeam { get; set; }

        public Score (string team1, string team2, int score1, int score2)
        {
            FirstTeamScore = score1;
            SecondTeamScore = score2;
            FirstTeam = team1;
            SecondTeam = team2;
        }
        public override bool Equals(object obj)
        {
            return Equals(obj as Score);
        }

        public bool Equals(Score other)
        {
            return other != null &&
                   FirstTeamScore == other.FirstTeamScore &&
                   SecondTeamScore == other.SecondTeamScore &&
                   FirstTeam == other.FirstTeam &&
                   SecondTeam == other.SecondTeam;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(FirstTeamScore, SecondTeamScore, FirstTeam, SecondTeam);
        }
    }
}
