﻿using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using BBCProject.NewFolder;
using BBCProject.Pages;

namespace BBCProject.pages
{
    public class HomePage : BasePage
    {
        private readonly IWebDriver driver;

        [FindsBy(How = How.XPath, Using = "//nav//a[text()='News']")]
        private IWebElement newsTab;

        [FindsBy(How = How.XPath, Using = "//nav//a[text()='Sport']")]
        private IWebElement sportTab;

        public HomePage(IWebDriver driver) : base(driver) 
        {
            this.driver = driver;
        }

        public SportPage SportTabClick()
        {
            sportTab.Click();
            return new SportPage(driver);
        }

        public NewsPage NewsTabClick() 
        {
            newsTab.Click();
            return new NewsPage(driver);
        }
    }
}
