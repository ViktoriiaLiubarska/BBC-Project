﻿using BBCProject.pages;
using BBCProject.Pages;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace BBCProject
{
    public class BusinessLogicLayer
    {
        private IWebDriver driver;

        public BusinessLogicLayer(IWebDriver driver)
        {
            this.driver = driver;
        }

        public void SubmitStoryToBBC(Dictionary<string, string> values)
        {
            new HomePage(driver).NewsTabClick()
                .CoronavirusTabClick()
                .YourCoronavirusStoriesClick()
                .HowToShareWithBBCTitleClick()
                .FillForm(values);
        }

        public void SearchForTextOfCategoryLinkOfHeadlineArticleOnNewsPage()
        {
            new HomePage(driver).NewsTabClick()
                .Search(new NewsPage(driver).getCategory());
        }

        public ScoresFixturesPage SearchForChampionshipScoreBoard(string championship, string month)
        {
            return new HomePage(driver).SportTabClick()
                .FootballTabClick()
                .ScoresFixturesTabClick()
                .Search(championship)
                .SelectMonth(month);
        }
    }
}
