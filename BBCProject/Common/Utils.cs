﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BBCProject
{
    public class Utils
    {
        public static bool IsListsEqual(List<string> list1, List<string> list2)
        {
            if (list1.Count() != list2.Count())
                return false;
            for (int i = 0; i < list1.Count(); i++)
            {
                if (!list2.Contains(list1[i]))
                    return false;
            }
            return true;
        }

        public static void ImplicitWait(IWebDriver driver, long timeToWait)
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(timeToWait);
        }

        public static void WaitForPageLoadComplete(IWebDriver driver, long timeToWait)
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(timeToWait)).Until(webDriver => ((IJavaScriptExecutor)webDriver).
                ExecuteScript("return document.readyState").Equals("complete"));
            ImplicitWait(driver, 0);
        }

        public static void WaitVisibilityOfElement(IWebDriver driver, long timeToWait, string element_xpath)
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(timeToWait)).Until(SeleniumExtras.WaitHelpers.
                ExpectedConditions.ElementIsVisible(By.XPath(element_xpath)));

        }
    }
}
