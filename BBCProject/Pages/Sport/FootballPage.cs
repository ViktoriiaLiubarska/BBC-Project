﻿using BBCProject.NewFolder;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace BBCProject.Pages
{
    public class FootballPage : BasePage
    {
        private readonly IWebDriver driver;

        [FindsBy(How = How.XPath, Using = "//a[@data-stat-title='Scores & Fixtures']")]
        private IWebElement scoresFixturesTab;

        public FootballPage(IWebDriver driver) : base(driver) 
        {
            this.driver = driver;
        }

        public ScoresFixturesPage ScoresFixturesTabClick()
        {
            scoresFixturesTab.Click();
            return new ScoresFixturesPage(driver);
        }
    }
}
