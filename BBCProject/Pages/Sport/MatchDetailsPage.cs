﻿using BBCProject.NewFolder;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System.Collections.Generic;

namespace BBCProject.Pages
{
    public class MatchDetailsPage : BasePage
    {
        private readonly IWebDriver driver;
        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'event-header')]")]
        private IWebElement match;

        public MatchDetailsPage(IWebDriver driver) : base(driver) 
        {
            this.driver = driver;
        }

        public Score GetScore()
        {
            Score score;
            Utils.ImplicitWait(driver, 0);
            var firstTeam = match.FindElement(By.XPath(".//span[contains(@class,'team-name--home')]//span[text()]")).Text;
            var firstTeamScore = match.FindElement(By.XPath(".//span[contains(@class,'number--home')]")).Text;
            var secondTeam = match.FindElement(By.XPath(".//span[contains(@class,'team-name--away')]//span[text()]")).Text;
            var secondTeamScore = match.FindElement(By.XPath(".//span[contains(@class,'number--away')]")).Text;
            score = new Score(firstTeam, secondTeam, int.Parse(firstTeamScore), int.Parse(secondTeamScore));
            return score;
        }
    }

}
