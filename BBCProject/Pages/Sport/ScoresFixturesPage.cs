﻿using BBCProject.NewFolder;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace BBCProject.Pages
{
    public class ScoresFixturesPage : BasePage
    {

        private readonly IWebDriver driver;
        private ScoreBoard board;

        [FindsBy(How = How.Id, Using = "downshift-0-input")]
        private IWebElement searchCompetitionInput;

        public ScoresFixturesPage(IWebDriver driver) : base(driver) 
        {
            this.driver = driver;
            board = new ScoreBoard(driver);
        }

        public ScoresFixturesPage Search(string textToSearch)
        {
            searchCompetitionInput.SendKeys(textToSearch + Keys.Down + Keys.Enter);
            return this;
        }

        public ScoresFixturesPage SelectMonth(string monthToSelect)
        {
            Utils.WaitForPageLoadComplete(driver, 30);
            driver.FindElement(By.XPath("//a[contains(@href,'" + monthToSelect + "')]")).Click();
            
            return this;
        }

        public Score GetScore(string team1, string team2)
        {
            return board.GetScore(team1, team2);
        }

        public MatchDetailsPage MatchClick(string team1, string team2)
        {
            board.getMatch(team1, team2).Click();
            return new MatchDetailsPage(driver);

        }
    }
}
