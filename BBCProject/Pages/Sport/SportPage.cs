﻿using BBCProject.NewFolder;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace BBCProject.Pages
{
    public class SportPage : BasePage
    {
        private readonly IWebDriver driver;

        [FindsBy(How = How.XPath, Using = "//div[@role='menubar']//a[@data-stat-title='Football']")]
        private IWebElement footballTab;

        public SportPage(IWebDriver driver) : base(driver) 
        {
            this.driver = driver;
            new Popup(driver).Close();
        }

        public FootballPage FootballTabClick()
        {
            footballTab.Click();
            return new FootballPage(driver);
        }
    }
}
