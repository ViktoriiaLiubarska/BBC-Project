﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BBCProject.NewFolder;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace BBCProject.pages
{
    public class NewsPage : BasePage
    {
        private readonly IWebDriver driver;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'inline-block@m')]//h3")]
        private IWebElement headlineArticle;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'auto gs-c-promo')]//h3")]
        private IList<IWebElement> secondaryArticles;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'inline-block@m')]//a[contains(@class,'no-visited-state')]//span")]
        private IWebElement category;

        [FindsBy(How = How.Id, Using = "orb-search-q")]
        private IWebElement searchInput;

        [FindsBy(How = How.XPath, Using = "//li[contains(@class,'menuitem')]//a[contains(@href,'coronavirus')]")]
        private IWebElement coronavirusTab;

        public NewsPage(IWebDriver driver) : base(driver)
        {
            this.driver = driver;
            new Popup(driver).Close();
        }

        public string getCategory()
        {
            return category.Text;
        }
        
        public string getHeadlineArticleTitle()
        {
            return headlineArticle.Text;
        }

        public List<string> SecondaryArticlesTitles()
        {
            List<string> result = new List<string>();
            foreach (var element in secondaryArticles)
            {
                result.Add(element.Text);
            }
            return result;
        }

        public CoronavirusPage CoronavirusTabClick()
        {
            coronavirusTab.Click();
            return new CoronavirusPage(driver);
        }

        public SearchPage Search(string textToSearch)
        {
            searchInput.SendKeys(textToSearch);
            searchInput.SendKeys(Keys.Enter);
            return new SearchPage(driver);
        }
    }
}
