﻿using BBCProject.NewFolder;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace BBCProject.pages
{
    public class HowToShareWithBBCPage : BasePage
    {
        private Form form;

        public HowToShareWithBBCPage(IWebDriver driver) : base(driver)
        {
            form = new Form(driver);
        }

        public void FillForm(Dictionary<string, string> values)
        {
           form.FillForm(values);
        }

        public Dictionary<string, string> GetValidationErrors(List<string> formElements)
        {
            return form.GetValidationErrors(formElements);
        }


    }
}
