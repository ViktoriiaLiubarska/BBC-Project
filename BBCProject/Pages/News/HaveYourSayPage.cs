﻿using BBCProject.NewFolder;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace BBCProject.pages
{
    public class HaveYourSayPage : BasePage
    {
        private readonly IWebDriver driver;

        [FindsBy(How = How.XPath, Using = "//a[contains(@href,'10725415')]")]
        private IWebElement howToShareWithBBCTitle;
        
        public HaveYourSayPage(IWebDriver driver) : base(driver) 
        {
            this.driver = driver;
        }

        public HowToShareWithBBCPage HowToShareWithBBCTitleClick()
        {
            howToShareWithBBCTitle.Click();
            return new HowToShareWithBBCPage(driver);
        }
    }
}
