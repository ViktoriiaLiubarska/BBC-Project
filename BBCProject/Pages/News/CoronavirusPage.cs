﻿using BBCProject.NewFolder;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace BBCProject.pages
{
    public class CoronavirusPage : BasePage
    {
        private readonly IWebDriver driver;

        [FindsBy(How = How.XPath, Using = "//li[contains(@class,'secondary-menuitem')]")]
        private IWebElement yourCoronavirusStoriesTab;
        
        public CoronavirusPage(IWebDriver driver) : base(driver) 
        {
            this.driver = driver;
        }

        public HaveYourSayPage YourCoronavirusStoriesClick()
        {
            yourCoronavirusStoriesTab.Click();
            return new HaveYourSayPage(driver);
        }

    }
}
