﻿using System;
using System.Collections.Generic;
using System.Text;
using BBCProject.NewFolder;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace BBCProject.pages
{
    public class SearchPage : BasePage
    {
        [FindsBy(How = How.XPath, Using = "(//p[contains(@class,'Promo')]//span)[1]")]
        private IWebElement firstArticle;

        public SearchPage(IWebDriver driver) : base(driver) { }

        public string getFirstArticleTitle()
        {
            return firstArticle.Text;
        }
    }
}
