﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace BBCProject.Hooks
{
    [Binding]
    public sealed class Hooks
    {
        private IWebDriver driver = Webdriver.Driver;

        [BeforeScenario]
        public void BeforeScenario()
        {
            driver.Navigate().GoToUrl("https://www.bbc.com");
        }

        [AfterScenario]
        public void AfterScenario()
        {
            Webdriver.Quit();
        }
    }
}
