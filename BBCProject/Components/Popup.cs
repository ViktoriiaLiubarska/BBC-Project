﻿using OpenQA.Selenium;
using System.Collections.Generic;

namespace BBCProject
{
    public class Popup
    {

        private readonly IWebDriver driver;
        private IList<IWebElement> maybeLaterButton;

        public Popup(IWebDriver driver)
        {
            this.driver = driver;
            Utils.ImplicitWait(driver, 5);
            maybeLaterButton = driver.FindElements(By.XPath("//button[@class='sign_in-exit']"));
            Utils.ImplicitWait(driver, 10);
        }

        public void Close()
        {
            if (maybeLaterButton.Count!=0)
            maybeLaterButton[0].Click();
        }
    }
}
