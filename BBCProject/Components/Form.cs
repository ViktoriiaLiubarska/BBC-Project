﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BBCProject
{
    public class Form
    {
        private readonly IWebDriver driver;

        [FindsBy(How = How.XPath, Using = "//button[@class='button']")]
        private IWebElement submitButton;

        public Form(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        public void FillForm(Dictionary<string, string> values)
        {
            Utils.WaitForPageLoadComplete(driver, 40);

            foreach (var element in values)
            {
                IList<IWebElement> textField = driver.FindElements(By.XPath("//*[@placeholder='" + element.Key + "']"));

                if (textField.Count() != 0)
                {
                    textField[0].SendKeys(element.Value);
                }
                else if (element.Value == "yes")
                {
                    IWebElement checkbox = driver.FindElement(By.XPath("//div[@class='checkbox']//p[contains(text(),'"
                        + element.Key + "')]"));
                    checkbox.Click();
                }
            }
            submitButton.Click();
        }

        public Dictionary<string, string> GetValidationErrors(List<string> formElements)
        {
            Utils.WaitVisibilityOfElement(driver, 40, "//div[@class='input-error-message']");
            
            var validationErrors = new Dictionary<string, string>();

            foreach (var element in formElements)
            {
                IList<IWebElement> textFieldError = driver.FindElements(By.XPath("//*[@placeholder='" 
                    + element + "']/following-sibling::div[@class='input-error-message']"));

                if (textFieldError.Count() != 0)
                {
                    validationErrors.Add(element, textFieldError[0].Text);
                }
                else  
                {
                    IList<IWebElement> checkboxError = driver.FindElements(By.XPath("//p[contains(text(),'" 
                        + element + "')]//ancestor::div[@class='checkbox']//div[@class='input-error-message']"));
                    if (checkboxError.Count() != 0)
                    {
                        validationErrors.Add(element, checkboxError[0].Text);
                    }
                }
            }
            return validationErrors;
        }
    }
}
