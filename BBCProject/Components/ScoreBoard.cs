﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;

namespace BBCProject
{
    public class ScoreBoard
    {
        private readonly IWebDriver driver;

        private IWebElement match;
        public ScoreBoard(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }
        public Score GetScore(string team1, string team2)
        {
            Score score;
            Utils.WaitVisibilityOfElement(driver, 30, "//article[@class='sp-c-fixture']");
            match = driver.FindElement(By.XPath($"//article[@class='sp-c-fixture'"  +
                $"and .//span[text()='{team1}'] and .//span[text()='{team2}']]"));
            var firstTeam = match.FindElement(By.XPath(".//span[contains(@class,'team-name--home')]//span[text()]")).Text;
            var firstTeamScore = match.FindElement(By.XPath(".//span[contains(@class,'number--home')]")).Text;
            var secondTeam = match.FindElement(By.XPath(".//span[contains(@class,'team-name--away')]//span[text()]")).Text;
            var secondTeamScore = match.FindElement(By.XPath(".//span[contains(@class,'number--away')]")).Text;
            score = new Score (firstTeam, secondTeam, int.Parse(firstTeamScore), int.Parse(secondTeamScore));
            return score;
        }

        public IWebElement getMatch(string team1, string team2)
        {
            Utils.WaitVisibilityOfElement(driver, 30, "//article[@class='sp-c-fixture']");
            if (match == null)
                match = driver.FindElement(By.XPath($"//article[@class='sp-c-fixture'" +
                $"and .//span[text()='{team1}'] and .//span[text()='{team2}']]"));
            return match;
        }
    }
}
