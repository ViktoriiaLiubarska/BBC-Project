#BBC

TA of Web applications with Selenium;

#Task1
1. Add a test that: 
   - Goes to BBC
   - Clicks on News
   - Checks the name of the headline article (the one with biggest picture and text) against a value specified in your test (hard-coded)

2. Add another test:
   - Goes to BBC
   - Clicks on News   
   - Checks secondary article titles (the ones to the right and below the headline article) against a List<String> specified in your test (hard-coded).
Imagine that you are testing the BBC site on a test environment, and you know what the titles will be. Your test, then, checks that these titles are in fact present on the page. 
The test should pass if all the titles are found, and fail if they are not found. It's normal that your test will fail the next day - this would not happen if we had a Test environment for BBC, with static database.

3. Write a test that: 
   - Goes to BBC
   - Clicks on News 
   - Stores the text of the Category link of the headline article (e.g. World, Europe...)
   - Enter this text in the Search bar
   - Check the name of the first article against a specified value (hard-coded)

#Task2
1.	Add a test which verifies that user can submit a story to BBC: 
    - From BBC Home page go to News;
	- Navigate to any of the news stories (e.g. by clicking title)
    - Click More (the one on the *red* menu) -> Have your say;
    - Go to “How to share with BBC news”;
    - There’s a form at the bottom – fill it with user information, but leave "your story" field empty;
    - Click Send
	- Verify that we are still on the same page - submission did not work, as expected.
2.	Add 3 more tests (use copy-paste). They should do the same as the one we just wrote: leave another fields empty.

#Task3
1.	Add a test that verifies that team scores display correctly: 
    - From BBC Home page go to Sport;
    - Go to Football
    - Go to Scores and Fixtures
	- Search for a championship (e.g. Scottish Championship). This value should be in your test, so that another test can search for a different value.
    - Select results for a month. Again, the month should be in your test.
    - Verify that 2 specific teams (specified in your test) played with a specific score (again, specified in your test);
    - Click on one of the team’s names;
    - Verify that the score at the center of the screen is also the one from your test (same value). Note: you need text from :after element. Selenium doesn’t work with them properly – google how to get around that.

 
2.	Add 4 more tests (use copy-paste). They should all do the same thing with different data (championship, month, teams, score).


